# Social+
To run this project locally, you will need NPM on your machine. See https://nodejs.org/en/download/.

After cloning this repository, open the repository folder on terminal and run `npm i`. Then run `npm run dev` and you should be directed to http://localhost:3000 on your default browser.

## Docs

Inside the src folder, you can find four folders: components, images, pages, and services.

### Components
This is where individual components are stored.

Cards contain all components on social media and online media dashboards, such as "User Location" and "News Sample".

Charts contain bar charts, line charts, and doughnut charts that you see on the dashboards.

Footer is the copyright and contact links on the bottom of the page.

Map is where the code for user location map is stored.

Navbar is the navigation bar on top of every page.

Sidebar is the side navbar on the left side of dashboards.

Tables contain all tables that you see on dashboards.

### Images
Logos and icons that are not from React libraries are stored here.

### Pages
Each page has a folder here: Add Topic Page, Dashboard (Social Media) Page, Login Page, Media (Online Media) Page, Profile Page, Register Page, Topic List Page, and Upgrade Membership Page. The file "index.js" is the source code for every page.

**To change the bank account number, go to pages/UpgradeMembership/payment.js and find [no rekening] in the return statement.**