const config = {
    production: {
      API_BASE_URL: 'https://social-plus-backend.herokuapp.com/',
      BASE_URL: '/',
      GOOGLE_CLIENT_ID: '14517191666-2a7778qi3ait0c1ecoiaev7kbbaotc81.apps.googleusercontent.com'
    },
    staging: {
      API_BASE_URL: 'https://social-plus-backend.herokuapp.com/',
      BASE_URL: '/',
      GOOGLE_CLIENT_ID: '14517191666-2a7778qi3ait0c1ecoiaev7kbbaotc81.apps.googleusercontent.com'
    },
    development: {
      API_BASE_URL: 'https://social-plus-backend.herokuapp.com/',
      BASE_URL: '/',
      GOOGLE_CLIENT_ID: '14517191666-2a7778qi3ait0c1ecoiaev7kbbaotc81.apps.googleusercontent.com'
    }
  };
  
  export default {
    ...config[process.env.NODE_ENV || 'development']
  };