import React, { useEffect, useState } from 'react'
import HashLoader from 'react-spinners/HashLoader'
import { AddTopicCard, TopicCard } from '../../components/Cards'
import '../../App.css'
import Navbar from '../../components/Navbar'
import { axiosInstance } from '../../services/api'

function TopicList() {
    const [loading, setLoading] = useState(true);
    const [topics, setTopics] = useState([]);
    const [remainingTopics, setRemainingTopics] = useState(0);

    const getTopics = async() => {
        try {
            setLoading(true)
            const res = await axiosInstance.get('topic/input/')
            console.log(res)
            setTopics(res.data.data.topic_list)
            setRemainingTopics(res.data.data.number_of_topics)
            console.log(topics)
            setLoading(false)
        } catch(err) {
            console.log(err)
        }
    }
    
    useEffect(() => {
        getTopics();
    }, []);

    return (
        <>
            <Navbar />
            <div className='home'>
                {topics.map((item) =>
                    <TopicCard topic={item.topic} keywords={item.keyword} id={item.id} />
                )}
                {remainingTopics > 0 ? 
                    <AddTopicCard /> : undefined
                }
            </div>
            {loading?
                <div style={{
                    marginTop: '55px',
                    position: 'fixed',
                    height: '100%',
                    width: '100%',
                    zIndex: '998',
                    top: '0',
                    left: '0',
                    backgroundColor: '#f6f8fa',
                    textAlign: 'center',
                }}>
                    <div style={{ marginTop: '100px' }}>
                        <HashLoader color='#4847f6' loading={loading} size={100} />
                    </div>
                </div> : undefined
            }
        </>
    )
}

export default TopicList
