import React, { useState, useEffect } from 'react'
import { Button, Grid, Paper } from '@material-ui/core'
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import HashLoader from 'react-spinners/HashLoader'
import Navbar from '../../components/Navbar'
import { axiosInstance } from '../../services/api'

function Profile() {
    const [loading, setLoading] = useState(true)
    const [user, setUser] = useState({
        fullname: '',
        username: '',
        email: '',
        occupation: '',
        membership: ''
    })
    const [payment, setPayment] = useState({payment_status: ''})

    const getProfile = async() => {
        try {
            setLoading(true)
            const res = await axiosInstance.get('auth/profile/')
            console.log(res)
            setUser(res.data.data.user)
            setPayment(res.data.data.payment)
            setLoading(false)
        } catch(err) {
            console.log(err)
        }
    }
    
    useEffect(() => {
        getProfile();
    }, []);

    return (
        <>
            <Navbar />
            <div className='home'>
                <Grid className='pageCenter'>
                    <Paper className='paperStyle'>
                        <h2 style={{ textAlign: 'center' }}>
                            <AccountCircleIcon fontSize='large' />
                        </h2>
                        <h3>Name</h3>
                        <p>{user.fullname}</p>
                        <h3>Username</h3>
                        <p>{user.username}</p>
                        <h3>Email</h3>
                        <p>{user.email}</p>
                        <h3>Occupation</h3>
                        <p>{user.occupation}</p>
                        <h3>Plan</h3>
                        <p>{user.membership}</p>
                        {/* {user.membership === 'Free' && payment.payment_status !== 'Pending' ?
                            <div style={{ marginTop: '2%', textAlign: 'center' }}>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={() => {window.location.pathname = '/upgrade/'}}
                                >
                                    Upgrade Membership
                                </Button>
                            </div> :
                            undefined
                        }
                        {payment.payment_status === 'Pending' ?
                            <div style={{ marginTop: '2%', textAlign: 'center' }}>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={() => {window.location.pathname = '/upgrade/payment/'}}
                                >
                                    Go to Payment
                                </Button>
                            </div> :
                            undefined
                        } */}
                    </Paper>
                </Grid>
            </div>
            {loading?
                <div style={{
                    marginTop: '55px',
                    position: 'fixed',
                    height: '100%',
                    width: '100%',
                    zIndex: '998',
                    top: '0',
                    left: '0',
                    backgroundColor: '#f6f8fa',
                    textAlign: 'center',
                }}>
                    <div style={{ marginTop: '100px' }}>
                        <HashLoader color='#4847f6' loading={loading} size={100} />
                    </div>
                </div> : undefined
            }
        </>
    )
}

export default Profile
