import React, { useEffect, useState } from 'react';
import HashLoader from 'react-spinners/HashLoader';
import { AgeCard, ExposureCard, GenderCard, ImpressionCard, KeyOpinionLeaderCard, LatestTweetsCard, LocationCard, MostActiveUsersCard, NGramCard, SentimentCard, TotalReachCard } from '../../components/Cards'
import Sidebar from '../../components/Sidebar'
import '../../App.css'
import Navbar from '../../components/Navbar'
import { axiosInstance } from '../../services/api'
import { useParams } from 'react-router-dom'
import { Button, TextField } from '@material-ui/core';

function Dashboard() {
    const { id } = useParams();
    
    const [loading, setLoading] = useState(true);

    const [socialMediaData, setSocialMediaData] = useState({
        title: '',
        keyword: [],
        impression: 0,
        total_reach: 0,
        sentiment: {
            positive: 0,
            positive_percentage: 0,
            neutral: 0,
            neutral_percentage: 0,
            negative: 0,
            negative_percentage: 0
        },
        daily_exposure: [],
        latest_tweets: [],
        age_profile: [],
        gender_profile: {
            Female: 0,
            Male:0
        },
        location: {
            top_provinces: [],
            top_cities: []
        },
        key_opinion_leader: {
            positive: 0,
            negative: 0
        },
        most_active_users: {
            positive: 0,
            negative: 0
        },
        n_gram: []
    })

    const sevenDays = Date.now() -((24*60*60*1000) * 7)

    const [form, setForm] = useState({
        start_date: new Date(sevenDays).toISOString().split('T')[0],
        end_date: new Date().toISOString().split('T')[0]
    });
    const handleChange = e => {
        setForm({ ...form, [e.target.name]: e.target.value})
    }
    
    const getData = async() => {
        try {
            setLoading(true)
            const res = await axiosInstance.post('topic/dashboard-summary/', {
                topic_id: id,
                dashboard_type: 'Social Media',
                start_date: form.start_date,
                end_date: form.end_date
            })
            console.log(res)
            setSocialMediaData(res.data)
            setLoading(false)
        } catch(err) {
            console.log(err)
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = e => {
        getData();
    }

    return (
        <>
            <Navbar />
            <Sidebar id={id} />
            <div className='page'>
                <h1>{socialMediaData.title}</h1>
                <div className='keywordList'>
                    {socialMediaData.keyword.map((keyword) =>
                        <p className='keyword'>#{keyword}</p>
                    )}
                </div>
                <form className='dateForm'>
                    <TextField
                        label='From'
                        type='date'
                        name='start_date'
                        value ={form.start_date}
                        onChange={handleChange}
                    />
                    <TextField
                        label='To'
                        type='date'
                        name='end_date'
                        style={{ marginLeft: '10px' }}
                        value ={form.end_date}
                        onChange={handleChange}
                    />
                    <Button
                        variant='contained'
                        style={{
                            margin: '10px auto auto 10px',
                            backgroundColor: 'white',
                            border: '1px solid',
                            borderColor: '#3F00FF',
                            color: '#3F00FF'
                        }}
                        onClick={handleSubmit}
                    >
                        Apply Filter
                    </Button>
                </form>
                <div className='dashboard'>
                    <ImpressionCard number={socialMediaData.impression} />
                    <TotalReachCard number={socialMediaData.total_reach} />
                    <SentimentCard
                        positive={socialMediaData.sentiment.positive}
                        posPercentage={socialMediaData.sentiment.positive_percentage}
                        negative={socialMediaData.sentiment.negative}
                        negPercentage={socialMediaData.sentiment.negative_percentage}
                        neutral={socialMediaData.sentiment.neutral}
                        neutPercentage={socialMediaData.sentiment.neutral_percentage}
                    />
                </div>
                <ExposureCard exposureData={socialMediaData.daily_exposure} />
                <LatestTweetsCard latestTweets={socialMediaData.latest_tweets} />
                <div className='dashboard'>
                    <AgeCard ageProfile={socialMediaData.age_profile} />
                    <GenderCard femaleNumber={socialMediaData.gender_profile.Female} maleNumber={socialMediaData.gender_profile.Male} />
                </div>
                <LocationCard
                    topProvinces={socialMediaData.location.top_provinces}
                    topCities={socialMediaData.location.top_cities}
                />
                <KeyOpinionLeaderCard
                    positiveKOL={socialMediaData.key_opinion_leader.positive}
                    negativeKOL={socialMediaData.key_opinion_leader.negative}
                />
                <MostActiveUsersCard
                    positiveMAU={socialMediaData.most_active_users.positive}
                    negativeMAU={socialMediaData.most_active_users.negative}
                />
                <NGramCard page='dashboard' nGramData={socialMediaData.n_gram} />
            </div>
            {loading?
                <div className='loadingPage'>
                    <div style={{ marginTop: '100px' }}>
                        <HashLoader color='#4847f6' loading={loading} size={100} />
                    </div>
                </div> : undefined
            }
        </>
    )
}

export default Dashboard
