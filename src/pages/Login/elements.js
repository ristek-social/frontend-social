import React, { useState } from 'react'
import { useHistory } from 'react-router'
import { createTheme } from '@material-ui/core/styles'
import { Button, Grid, Paper, TextField, ThemeProvider } from '@material-ui/core'
import Cookies from 'js-cookie'
import GoogleLogin from 'react-google-login'
import { authInstance } from '../../services/api'
import config from '../../config'
import logo from '../../images/logo1.png'

const theme = createTheme({
    overrides: {
      MuiInput: {
        root: {
            borderBottom: '1px solid white',
            borderBottomColor: 'white',
            color: "white",
            "&$before": {
                borderBottom: '1px solid white',
                borderBottomColor: 'white',
                color: "white"
            },
            "&$after": {
                borderBottom: '1px solid white',
                borderBottomColor: 'white',
                color: "white"
            },
            "&$focused": {
                borderBottom: '1px solid white',
                borderBottomColor: 'white',
                color: "white"
            }
        }
      },
      MuiInputLabel: {
        root: {
          color: "white",
          "&$focused": {
            color: "white"
          }
        }
      }
    }
});

const LoginElement = () => {
    const textFieldStyle = {
        margin: '10px auto',
    }

    const red = {
        color: 'red'
    }

    const history = useHistory()

    const [form, setForm] = useState({});

    const [emailValid, setEmailValid] = useState(true);
    const [passwordValid, setPasswordValid] = useState(true);
    const [formError, setFormError] = useState({});
    const [submitError, setSubmitError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    
    const validateEmail = (e) => {
        e.preventDefault();
        if (e.target.value.length === 0) {
            setEmailValid(false);
            setFormError({ ...formError, [e.target.name]: 'Email cannot be empty' })
        } else if (!e.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
            setEmailValid(false);
            setFormError({ ...formError, [e.target.name]: 'Email is invalid' })
        } else {
            setEmailValid(true);
        }
    }

    const validatePassword = (e) => {
        e.preventDefault();
        if (e.target.value.length > 0) {
            setPasswordValid(true);
        } else {
            setPasswordValid(false);
            setFormError({ ...formError, [e.target.name]: 'Password cannot be empty' })
        }
    }

    const handleChange = e => {
        setForm({ ...form, [e.target.name]: e.target.value})
    }

    const [loading, setLoading] = useState(false)
    
    const handleSubmit = (e) => {
        e.preventDefault();

        const filled = form.email && form.password;

        if (emailValid && passwordValid && filled) {
            setLoading(true);
            setSubmitError(false);
            
            authInstance
                .post('auth/login/', {
                    email: form.email,
                    password: form.password,
                })
                .then((res) => {
                    const accessToken = res.data.data.tokens.access
                    const refreshToken = res.data.data.tokens.refresh
                    Cookies.set('access', accessToken)
                    Cookies.set('refresh', refreshToken)
                    Cookies.set('login_type', 'email')
                    history.push('/')
                })
                .catch((err) => {
                    console.log(err);
                    if (err.response.status === 401) {
                        if (err.response.data.errors.detail === 'Email is not verified') {
                            setErrorMessage('Please activate your account first')
                        } else {
                            setErrorMessage('Wrong email or password.')
                        }
                    } else {
                        setErrorMessage(err.response.data.errors.detail)
                    }
                    setLoading(false);
                    setForm({});
                    setSubmitError(true);
                });
        }
    }

    const responseGoogle = (response) => {
        console.log(response);
        const token = response.tokenId;
        authInstance
            .post('social-auth/google/', {
                auth_token: token
            })
            .then((res) => {
                console.log(res)
                const accessToken = res.data.tokens.access
                const refreshToken = res.data.tokens.refresh
                Cookies.set('access', accessToken)
                Cookies.set('refresh', refreshToken)
                Cookies.set('login_type', 'google')
                history.push('/')
            })
            .catch((err) => {
                console.log(err)
            });
    }

    return (
        <div className='loginPage'>
            <Grid align='center' className='pageCenter'>
                <img src={logo} alt='logo' style={{ height: '45px', marginTop: '5px' }}/>
                <Paper className='loginCard'>
                    <h2 style={{ color: 'white' }}>
                        Log In
                    </h2>
                    <form>
                        <ThemeProvider theme={theme}>
                            {!emailValid ?
                                <TextField
                                    fullWidth
                                    value={form.email || ''}
                                    onChange={handleChange}
                                    onBlur={validateEmail}
                                    required
                                    name='email'
                                    label='Email'
                                    style={textFieldStyle}
                                    error
                                    helperText={formError.email}
                                /> :
                                <TextField
                                    fullWidth
                                    value={form.email || ''}
                                    onChange={handleChange}
                                    onBlur={validateEmail}
                                    required
                                    name='email'
                                    label='Email'
                                    style={textFieldStyle}
                                />
                            }
                            {!passwordValid ? 
                                <TextField
                                    fullWidth
                                    value={form.password || ''}
                                    onChange={handleChange}
                                    onBlur={validatePassword}
                                    required
                                    name='password'
                                    label='Password'
                                    type='password'
                                    autoComplete='current-password'
                                    style={textFieldStyle}
                                    error
                                    helperText={formError.password}
                                /> :
                                <TextField
                                    fullWidth
                                    value={form.password || ''}
                                    onChange={handleChange}
                                    onBlur={validatePassword}
                                    required
                                    name='password'
                                    label='Password'
                                    type='password'
                                    autoComplete='current-password'
                                    style={textFieldStyle}
                                />
                            }
                        </ThemeProvider>
                        {submitError ?
                            <p style={red}>{errorMessage}</p> : undefined
                        }
                        {loading ?
                            <Button
                                type='submit'
                                variant='contained'
                                disabled
                                style={{ margin: '10px auto', backgroundColor: 'white', color: '#19248c' }}
                            >
                                Loading
                            </Button> :
                            <Button
                                type='submit'
                                variant='contained'
                                color='primary'
                                style={{ margin: '10px auto', backgroundColor: 'white', color: '#19248c' }}
                                onClick={handleSubmit}
                            >
                                Log In
                            </Button>
                        }
                    </form>
                    <GoogleLogin 
                        clientId={config.GOOGLE_CLIENT_ID}
                        buttonText='Login with Google'
                        onSuccess={responseGoogle}
                        onFailure={responseGoogle}
                        style={{ margin: '10px auto', width: '30%'}}
                    />
                    <p style={{ margin: '10px auto 10px auto', color: 'white' }}>Don't have an account yet? <a href='/register' style={{ color: 'white' }}>Register</a></p>
                </Paper>
            </Grid>
        </div>
    )
}

export default LoginElement
