import React from 'react'
import Navbar from '../../components/Navbar'
import LoginElement from './elements'

const Login = () => {
    return (
        <LoginElement />
    )
}

export default Login
