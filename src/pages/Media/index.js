import React, { useEffect, useState } from 'react'
import HashLoader from 'react-spinners/HashLoader'
import { LatestNewsCard, MediaCoverageCard, NewsTrendCard, NGramCard, TotalNewsCard, NewsSentimentCard } from '../../components/Cards'
import Sidebar from '../../components/Sidebar'
import '../../App.css'
import Navbar from '../../components/Navbar'
import { useParams } from 'react-router-dom'
import { Button, TextField } from '@material-ui/core'
import { axiosInstance } from '../../services/api'

function Media() {
    const { id } = useParams();

    const [loading, setLoading] = useState(true);

    const [onlineMediaData, setOnlineMediaData] = useState({
        title: '',
        keyword: [],
        total_news: 0,
        sentiment: {
            positive: 0,
            positive_percentage: 0,
            neutral: 0,
            neutral_percentage: 0,
            negative: 0,
            negative_percentage: 0
        },
        news_trend: [],
        latest_news: [],
        n_gram: [],
        media_coverage: []
    })

    const sevenDays = Date.now() -((24*60*60*1000) * 7)

    const [form, setForm] = useState({
        start_date: new Date(sevenDays).toISOString().split('T')[0],
        end_date: new Date().toISOString().split('T')[0]
    });
    const handleChange = e => {
        setForm({ ...form, [e.target.name]: e.target.value})
    }

    const getData = async() => {
        try {
            setLoading(true)
            const res = await axiosInstance.post('topic/dashboard-summary/', {
                topic_id: id,
                dashboard_type: 'Online Media',
                start_date: form.start_date,
                end_date: form.end_date
            })
            console.log(res)
            setOnlineMediaData(res.data)
            setLoading(false)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = e => {
        getData();
    }

    return (
        <>
            <Navbar />
            <Sidebar id={id} />
            <div className='page'>
                <h1>{onlineMediaData.title}</h1>
                <div>
                    {onlineMediaData.keyword.map((keyword) =>
                        <p className='keyword'>#{keyword}</p>
                    )}
                </div>
                <form className='dateForm'>
                    <TextField
                        label='From'
                        type='date'
                        name='start_date'
                        value ={form.start_date}
                        onChange={handleChange}
                    />
                    <TextField
                        label='To'
                        type='date'
                        name='end_date'
                        style={{ marginLeft: '10px' }}
                        value ={form.end_date}
                        onChange={handleChange}
                    />
                    <Button
                        variant='contained'
                        color='primary'
                        style={{
                            margin: '10px auto auto 10px',
                            backgroundColor: 'white',
                            border: '1px solid',
                            borderColor: '#3F00FF',
                            color: '#3F00FF'
                        }}
                        onClick={handleSubmit}
                    >
                        Apply Filter
                    </Button>
                </form>
                <div className='dashboard'>
                    <TotalNewsCard number={onlineMediaData.total_news} />
                    <NewsSentimentCard
                        positive={onlineMediaData.sentiment.positive}
                        posPercentage={onlineMediaData.sentiment.positive_percentage}
                        negative={onlineMediaData.sentiment.negative}
                        negPercentage={onlineMediaData.sentiment.negative_percentage}
                        neutral={onlineMediaData.sentiment.neutral}
                        neutPercentage={onlineMediaData.sentiment.neutral_percentage}
                    />
                </div>
                <NewsTrendCard newsTrend={onlineMediaData.news_trend} />
                <LatestNewsCard latestNews={onlineMediaData.latest_news} />
                <NGramCard page='media' nGramData={onlineMediaData.n_gram} />
                <MediaCoverageCard mediaCoverage={onlineMediaData.media_coverage} />
            </div>
            {loading?
                <div className='loadingPage'>
                    <div style={{ marginTop: '100px' }}>
                        <HashLoader color='#4847f6' loading={loading} size={100} />
                    </div>
                </div> : undefined
            }
        </>
    )
}

export default Media
