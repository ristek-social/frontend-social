import { Button, Grid, Paper, TextField } from '@material-ui/core'
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import '../../App.css'
import Navbar from '../../components/Navbar'
import { axiosInstance } from '../../services/api'

function AddTopic() {
    const [loading, setLoading] = useState(false)
    
    const textFieldStyle = {
        margin: '10px auto'
    }

    const [form, setForm] = useState({})

    const handleChange = e => {
        setForm({ ...form, [e.target.name]: e.target.value})
    }

    const history = useHistory()

    const handleSubmit = (e) => {
        e.preventDefault();
        if (form.topic && form.keywords) {
            setLoading(true)
            axiosInstance
                .post('topic/input/', {
                    topic: form.topic,
                    keyword: form.keywords
                })
                .then((res) => {
                    history.push('/')
                })
                .catch((err) => {
                    console.log(err)
                    setLoading(false)
                })
        }
    }

    return (
        <>
            <Navbar />
            <div className='home'>
                <Grid align='center' className='pageCenter'>
                    <Paper className='paperStyle'>
                        <form>
                        <TextField
                            fullWidth
                            variant='outlined'
                            value={form.topic || ''}
                            onChange={handleChange}
                            required
                            name='topic'
                            label='Topic Name'
                            style={textFieldStyle}
                        />
                        <TextField
                            fullWidth
                            variant='outlined'
                            value={form.keywords || ''}
                            onChange={handleChange}
                            required
                            name='keywords'
                            label='Keywords'
                            helperText='Write down your keywords, separated by commas e.g. "car, taxi, plane".'
                            style={textFieldStyle}
                        />
                        {loading ?
                            <Button
                                type='submit'
                                variant='contained'
                                // style={{ backgroundColor: '#4847f6', color: 'white', fontWeight: 'bold' }}
                                disabled
                            >
                                Loading
                            </Button> :
                            <Button
                                type='submit'
                                variant='contained'
                                style={{ backgroundColor: '#4847f6', color: 'white', fontWeight: 'bold' }}
                                onClick={handleSubmit}
                            >
                                Submit
                            </Button>
                        }
                        </form>
                    </Paper>
                </Grid>
            </div>
        </>
    )
}

export default AddTopic
