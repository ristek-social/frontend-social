import { Button, Grid, Paper } from '@material-ui/core'
import React from 'react'

const EmailVerification = () => {
    const email = localStorage.getItem('email')

    return (
        <>
            <div className='home'>
                <Grid align='center' className='pageCenter'>
                    <Paper className='paperStyle'>
                        <h3>We have sent a verification email to {email}.</h3>
                        <p>Please check your mailbox and click the link to verify your account.</p>
                        <Button
                            style={{ marginTop: '10px' }}
                            variant='contained'
                            color='primary'
                            onClick={() => {window.location.pathname = '/login/'}}
                        >
                            Log In
                        </Button>
                    </Paper>
                </Grid>
            </div>
        </>
    )
}

export default EmailVerification
