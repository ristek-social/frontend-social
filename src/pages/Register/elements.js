import React, { useState } from 'react'
import { useHistory } from 'react-router'
import { createTheme } from '@material-ui/core/styles'
import { Button, Grid, MenuItem, Paper, TextField, ThemeProvider } from '@material-ui/core'
import { authInstance } from '../../services/api'
import logo from '../../images/logo1.png'

const theme = createTheme({
    overrides: {
      MuiInput: {
        root: {
            borderBottom: '1px solid white',
            borderBottomColor: 'white',
            color: "white",
            "&$before": {
                borderBottom: '1px solid white',
                borderBottomColor: 'white',
                color: "white"
            },
            "&$after": {
                borderBottom: '1px solid white',
                borderBottomColor: 'white',
                color: "white"
            },
            "&$focused": {
                borderBottom: '1px solid white',
                borderBottomColor: 'white',
                color: "white"
            }
        }
      },
      MuiInputLabel: {
        root: {
          color: "white",
          "&$focused": {
            color: "white"
          }
        }
      }
    }
});

const RegisterElement = () => {
    const textFieldStyle = {
        margin: '10px auto',
        textAlign: 'left'
    }

    const red = {
        color: 'red'
    }

    const history = useHistory()

    const [form, setForm] = useState({});

    const [nameValid, setNameValid] = useState(true);
    const [usernameValid, setUsernameValid] = useState(true);
    const [emailValid, setEmailValid] = useState(true);
    const [occupationValid, setOccupationValid] = useState(true);
    const [passwordValid, setPasswordValid] = useState(true);
    const [formError, setFormError] = useState({});
    const [submitError, setSubmitError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const handleChange = e => {
        setForm({ ...form, [e.target.name]: e.target.value})
    }

    var [loading, setLoading] = useState(false)

    const validateName = (e) => {
        e.preventDefault();
        if (e.target.value.length > 0) {
            setNameValid(true);
        } else {
            setNameValid(false);
            setFormError({ ...formError, [e.target.name]: 'Name cannot be empty' })
        }
    }

    const validateUsername = (e) => {
        e.preventDefault();
        if (e.target.value.length > 0) {
            setUsernameValid(true);
        } else {
            setUsernameValid(false);
            setFormError({ ...formError, [e.target.name]: 'Username cannot be empty' })
        }
    }

    const validateEmail = (e) => {
        e.preventDefault();
        if (e.target.value.length === 0) {
            setEmailValid(false);
            setFormError({ ...formError, [e.target.name]: 'Email cannot be empty' })
        } else if (!e.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
            setEmailValid(false);
            setFormError({ ...formError, [e.target.name]: 'Email is invalid' })
        } else {
            setEmailValid(true);
        }
    }

    const validateOccupation = (e) => {
        e.preventDefault();
        if (e.target.value.length > 0) {
            setOccupationValid(true);
        } else {
            setOccupationValid(false);
            setFormError({ ...formError, [e.target.name]: 'Occupation cannot be empty' })
        }
    }

    const validatePassword = (e) => {
        e.preventDefault();
        if (e.target.value.length >= 8) {
            setPasswordValid(true);
        } else {
            setPasswordValid(false);
            setFormError({ ...formError, [e.target.name]: 'Password must be at least 8 characters' })
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        const filled = form.fullname && form.username && form.email && form.occupation && form.password;
        
        if (nameValid && usernameValid && emailValid && occupationValid && passwordValid && filled) {
            setLoading(true);
            setSubmitError(false);
            
            authInstance
                .post('auth/register/', {
                    fullname: form.fullname,
                    username: form.username,
                    email: form.email,
                    occupation: form.occupation,
                    password: form.password,
                })
                .then((res) => {
                    localStorage.setItem('email', form.email)
                    history.push('/verify')
                    console.log(res);
                    console.log(res.data);
                })
                .catch((err) => {
                    console.log(err);
                    if (err.response.status === 400) {
                        setErrorMessage('Email or username already taken.')
                    } else {
                        setErrorMessage('Sorry, an error occurred. Please try again later.')
                    }
                    setLoading(false);
                    setForm({});
                    setSubmitError(true);
                });
        }
    }

    return (
        <div className='loginPage'>
            <Grid align='center' className='pageCenter'>
                <img src={logo} alt='logo' style={{ height: '45px', marginTop: '5px' }}/>
                <Paper className='loginCard'>
                    <h2 style={{ color: 'white' }}>
                        Register
                    </h2>
                    <form>
                        <ThemeProvider theme={theme}>
                            {!nameValid ?
                                <TextField
                                    fullWidth
                                    value={form.fullname || ''}
                                    onChange={handleChange}
                                    onBlur={validateName}
                                    required
                                    name='fullname'
                                    label='Name'
                                    style={textFieldStyle}
                                    error
                                    helperText={formError.fullname}
                                /> : 
                                <TextField
                                    fullWidth
                                    value={form.fullname || ''}
                                    onChange={handleChange}
                                    onBlur={validateName}
                                    required
                                    name='fullname'
                                    label='Name'
                                    style={textFieldStyle}
                                />
                            }
                            {!usernameValid ?
                                <TextField
                                    fullWidth
                                    value={form.username || ''}
                                    onChange={handleChange}
                                    onBlur={validateUsername}
                                    required
                                    name='username'
                                    label='Username'
                                    style={textFieldStyle}
                                    error
                                    helperText={formError.username}
                                /> :
                                <TextField
                                    fullWidth
                                    value={form.username || ''}
                                    onChange={handleChange}
                                    onBlur={validateUsername}
                                    required
                                    name='username'
                                    label='Username'
                                    style={textFieldStyle}
                                />
                            }
                            {!emailValid ?
                                <TextField
                                    fullWidth
                                    value={form.email || ''}
                                    onChange={handleChange}
                                    onBlur={validateEmail}
                                    required
                                    name='email'
                                    label='Email'
                                    style={textFieldStyle}
                                    error
                                    helperText={formError.email}
                                /> :
                                <TextField
                                    fullWidth
                                    value={form.email || ''}
                                    onChange={handleChange}
                                    onBlur={validateEmail}
                                    required
                                    name='email'
                                    label='Email'
                                    style={textFieldStyle}
                                />
                            }
                            <TextField
                                fullWidth
                                select
                                value={form.occupation || ''}
                                onChange={handleChange}
                                onBlur={validateOccupation}
                                required
                                name='occupation'
                                label='Occupation'
                                style={textFieldStyle}
                            >
                                <MenuItem value={'CEO'}>CEO</MenuItem>
                                <MenuItem value={'CF'}>Co-Founder</MenuItem>
                                <MenuItem value={'PM'}>Product Manager</MenuItem>
                                <MenuItem value={'BD'}>Business Development</MenuItem>
                                <MenuItem value={'DS'}>Data Science</MenuItem>
                                <MenuItem value={'SE'}>Software Engineer</MenuItem>
                                <MenuItem value={'QA'}>Quality Assurance Engineer</MenuItem>
                                <MenuItem value={'DO'}>DevOps</MenuItem>
                                <MenuItem value={'DM'}>Digital Marketing</MenuItem>
                                <MenuItem value={'GD'}>Graphic Designer</MenuItem>
                                <MenuItem value={'S'}>Student/University Student</MenuItem>
                                <MenuItem value={'O'}>Other</MenuItem>
                            </TextField>
                            {!passwordValid ? 
                                <TextField
                                    fullWidth
                                    value={form.password || ''}
                                    onChange={handleChange}
                                    onBlur={validatePassword}
                                    required
                                    name='password'
                                    label='Password'
                                    type='password'
                                    autoComplete='current-password'
                                    style={textFieldStyle}
                                    error
                                    helperText={formError.password}
                                /> :
                                <TextField
                                    fullWidth
                                    value={form.password || ''}
                                    onChange={handleChange}
                                    onBlur={validatePassword}
                                    required
                                    name='password'
                                    label='Password'
                                    type='password'
                                    autoComplete='current-password'
                                    style={textFieldStyle}
                                />
                            }
                        </ThemeProvider>
                        {submitError ?
                            <p style={red}>{errorMessage}</p> : undefined
                        }
                        {loading ?
                            <Button
                                type='submit'
                                variant='contained'
                                disabled
                                style={{ margin: '10px auto', backgroundColor: 'white', color: '#19248c' }}
                            >
                                Loading
                            </Button> :
                            <Button
                                type='submit'
                                variant='contained'
                                color='primary'
                                style={{ margin: '10px auto', backgroundColor: 'white', color: '#19248c' }}
                                onClick={handleSubmit}
                            >
                                Sign Up
                            </Button>
                        }
                    </form>
                    <p style={{ color: 'white' }}>Have an account? <a href='/login' style={{ color: 'white' }}>Log In</a></p>
                </Paper>
            </Grid>
        </div>
    )
}

export default RegisterElement
