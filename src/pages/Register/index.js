import React from 'react'
import Navbar from '../../components/Navbar'
import RegisterElement from './elements'

const Register = () => {
    return (
        <RegisterElement />
    )
}

export default Register
