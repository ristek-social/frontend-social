import React, { useState, useEffect } from 'react'
import { Button, Card, Grid, Paper } from '@material-ui/core'
import HashLoader from 'react-spinners/HashLoader'
import { useHistory } from 'react-router-dom'
import Navbar from '../../components/Navbar'
import { axiosInstance } from '../../services/api'

function UpgradeMembership() {

    const history = useHistory()
    const [loading, setLoading] = useState(true)
    const [plans, setPlans] = useState([])

    const getPlans = async() => {
        try {
            setLoading(true)
            const res = await axiosInstance.get('auth/retrieve-membership-type/')
            console.log(res)
            const membership_list = res.data.data.membership_type_list
            for (let i = 0; i < membership_list.length; i++) {
                membership_list[i].price = (membership_list[i].price).toLocaleString('en-US', {
                    style: 'currency',
                    currency: 'IDR',
                })
            }
            setPlans(membership_list)
            setLoading(false)
        } catch(err) {
            console.log(err)
        }
    }

    const upgradeMembership = (id) => {
        axiosInstance
            .post('auth/request-upgrade-membership/', {
                new_membership: id,
                payment_type: 'BTR'
            })
            .then((res) => {
                console.log(res)
                history.push('/upgrade/payment/')
            })
            .catch((err) => {
                console.log(err)
            })
    }
    
    useEffect(() => {
        getPlans();
    }, []);

    return (
        <>
            <Navbar />
            <div className='home'>
                <Grid align='center' className='pageCenter'>
                    <Paper className='paperStyle'>
                        <h2>
                            Upgrade Membership
                        </h2>
                        {plans.map((item) =>
                            <Card style={{ width: '50%', margin: '3%', padding: '1%' }}>
                                <h3>{item.type}</h3>
                                <p>{item.price}</p>
                                <p>{item.number_of_topics} topic(s)</p>
                                {item.type !== 'Free' ?
                                    <Button
                                        style={{
                                            marginTop: '5px',
                                            backgroundColor: 'white',
                                            border: '1px solid',
                                            borderColor: '#3F00FF',
                                            color: '#3F00FF'
                                        }}
                                        onClick={() => upgradeMembership(item.id)}
                                    >
                                        Buy Plan
                                    </Button> :
                                    undefined
                                }
                            </Card>
                        )}
                    </Paper>
                </Grid>
            </div>
            {loading?
                <div style={{
                    marginTop: '55px',
                    position: 'fixed',
                    height: '100%',
                    width: '100%',
                    zIndex: '998',
                    top: '0',
                    left: '0',
                    backgroundColor: '#f6f8fa',
                    textAlign: 'center',
                }}>
                    <div style={{ marginTop: '100px' }}>
                        <HashLoader color='#4847f6' loading={loading} size={100} />
                    </div>
                </div> : undefined
            }
        </>
    )
}

export default UpgradeMembership
