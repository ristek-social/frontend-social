import React, { useState, useEffect } from 'react'
import { Grid, Paper } from '@material-ui/core'
import HashLoader from 'react-spinners/HashLoader'
import Navbar from '../../components/Navbar'
import { axiosInstance } from '../../services/api'

function Payment() {
    const [loading, setLoading] = useState(true)
    const [confirmation, setConfirmation] = useState({
        new_membership: '',
        amount: '',
        payment_type: '',
        payment_status: '',
        updated_at: ''
    })

    const getPayment = async() => {
        try {
            setLoading(true)
            const res = await axiosInstance.get('auth/profile/')
            console.log(res)
            const payment = res.data.data.payment
            payment.amount = (payment.amount)
                .toLocaleString('en-US', {
                    style: 'currency',
                    currency: 'IDR',
                })
            setConfirmation(res.data.data.payment)
            setLoading(false)
        } catch(err) {
            console.log(err)
        }
    }
    
    useEffect(() => {
        getPayment();
    }, []);

    return (
        <>
            <Navbar />
            <div className='home'>
                <Grid align='center' className='pageCenter'>
                    <Paper className='paperStyle'>
                        <h2>
                            Payment
                        </h2>
                        <p>Please make your payment to:</p>
                        <p>[no rekening]</p>
                        <div style={{ padding: '4%', color: 'white', backgroundColor: '#3F00FF', width: '80%', margin: '10px', borderRadius: '10px' }}>
                            <h3>Amount: <br/> {confirmation.amount}</h3>
                        </div>
                        <p>Send your payment receipt to contact@continuum.id with subject <strong>UPDATE MEMBERSHIP - USERNAME</strong> and your membership status should change when the confirmation is done.</p>
                        <p>Thank you!</p>
                    </Paper>
                </Grid>
            </div>
            {loading?
                <div style={{
                    marginTop: '55px',
                    position: 'fixed',
                    height: '100%',
                    width: '100%',
                    zIndex: '998',
                    top: '0',
                    left: '0',
                    backgroundColor: '#f6f8fa',
                    textAlign: 'center',
                }}>
                    <div style={{ marginTop: '100px' }}>
                        <HashLoader color='#4847f6' loading={loading} size={100} />
                    </div>
                </div> : undefined
            }
        </>
    )
}

export default Payment
