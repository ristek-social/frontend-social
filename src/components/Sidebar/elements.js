import styled from 'styled-components'

export const SidebarDiv = styled.div`
    height: 100vh;
    width: 18vw;
    margin-top: 55px;
    background-color: #2f4050;
    position: fixed;
    top: 0%;

    @media screen and (max-width: 700px) {
        width: 100%;
        height: auto;
        position: relative;
    }
`

export const SidebarList = styled.ul`
    height: auto;
    width: 100%;
    padding: 0%;
    @media screen and (max-width: 700px) {
        text-align: center;
    }
`

export const SidebarMenu = styled.li`
    width: 100%;
    height: 60px;
    list-style-type: none;
    margin: 0%;
    display: flex;
    flex-direction: row;
    color: #fff;
    justify-content: center;
    align-items: center;

    &:hover {
        cursor: pointer;
        background-color: #293846;
    }
`

export const SidebarIcon = styled.div`
    @media screen and (min-width: 700px) {
        flex: 30%;
        display: grid;
        place-items: center;
    }
    @media screen and (max-width: 700px) {
        visibility: hidden;
        width: 0;
    }
`

export const SidebarTitle = styled.div`
    flex: 70%;
    font-weight: 600;
`