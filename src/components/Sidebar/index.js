import React from 'react'
import '../../App.css'
import { SidebarDiv, SidebarIcon, SidebarList, SidebarMenu, SidebarTitle } from './elements'
import { SidebarData } from './data'

function Sidebar({ id }) {
    return (
        <SidebarDiv>
            <SidebarList>
                {SidebarData.map((val, key) => {
                    return (
                        <SidebarMenu
                            key={key}
                            onClick={() => {window.location.pathname = val.link + '/' + id}}
                            id={window.location.pathname === val.link + '/' + id ? 'active' : ''}
                        >
                            <SidebarIcon>{val.icon}</SidebarIcon>
                            <SidebarTitle>{val.title}</SidebarTitle>
                        </SidebarMenu>
                    )
                })}
            </SidebarList>
        </SidebarDiv>
    )
}

export default Sidebar
