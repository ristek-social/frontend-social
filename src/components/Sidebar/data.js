import React from 'react'
import TwitterIcon from '@material-ui/icons/Twitter';
import WebIcon from '@material-ui/icons/Web';

export const SidebarData = [
    {
        title: 'Social Media',
        icon: <TwitterIcon />,
        link: '/dashboard'
    },
    {
        title: 'Online Media',
        icon: <WebIcon />,
        link: '/media'
    }
]