import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { CardActionArea, Tooltip } from '@material-ui/core';
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import NavigateNextRoundedIcon from '@material-ui/icons/NavigateNextRounded';
import RecordVoiceOverRoundedIcon from '@material-ui/icons/RecordVoiceOverRounded';
import FaceRoundedIcon from '@material-ui/icons/FaceRounded';
import BarChartRoundedIcon from '@material-ui/icons/BarChartRounded';
import TwitterIcon from '@material-ui/icons/Twitter';
import WcRoundedIcon from '@material-ui/icons/WcRounded';
import LocationOnRoundedIcon from '@material-ui/icons/LocationOnRounded';
import PeopleOutlineRoundedIcon from '@material-ui/icons/PeopleOutlineRounded';
import EventIcon from '@material-ui/icons/Event';
import { TrendingUpRounded } from '@material-ui/icons';
import { AgeBarChart, ExposureLineChart, LocationBarChart, LocationDoughnutChart, MediaCoverageBarChart, NewsTrendLineChart, NGramBarChart } from '../Charts';
import LatestTweetsTable from '../Tables/latestTweets';
import TweetSampleTable from '../Tables/tweetSample';
import { PositiveReachTable, NegativeReachTable } from '../Tables/reachTable';
import LatestNewsTable from '../Tables/latestNews';
import ChoroplethMap from '../Map';
import NewsSampleTable from '../Tables/newsSample';
import female from '../../images/female.svg';
import male from '../../images/male.svg';

const LargeTooltip = withStyles((theme) => ({
    tooltip: {
        fontSize: 12
    }
}))(Tooltip);

export const TopicCard = ({ topic, keywords, id }) => {
    return (
        <Card variant='outlined' className='card'>
            <CardActionArea onClick={() => {window.location.pathname = '/dashboard/' + id}}>
                <CardContent className='topic'>
                    <div className='text'>
                        <Typography variant='body' component='h2'>
                            {topic}
                        </Typography>
                        <div>
                            {keywords.map((keyword) =>
                                <p className='cardKeyword'>#{keyword}</p>
                            )}
                        </div>
                    </div>
                    <NavigateNextRoundedIcon />
                </CardContent>
            </CardActionArea>
        </Card>
    );
}

export const AddTopicCard = () => {
    return (
        <Card variant='outlined' className='card'>
            <CardActionArea onClick={() => {window.location.pathname = '/add-topic'}}>
                <CardContent className='topic'>
                    <div className='text'>
                        <Typography variant='body' component='h2'>
                            Add More Topic
                        </Typography>
                    </div>
                    <AddRoundedIcon />
                </CardContent>
            </CardActionArea>
        </Card>
    );
}

export const ImpressionCard = ({ number }) => {
    return (
        <div className='smallCard'>
            <LargeTooltip title='Number of times your topic is displayed'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Impression⠀
                    </Typography>
                    <TrendingUpRounded fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined' className='top'>
                <CardContent>
                    <Typography variant='body' component='h1' style={{ fontSize: 72 }}>
                        {number}
                    </Typography>
                </CardContent>
            </Card>
        </div>
    );
}

export const TotalReachCard = ({ number }) => {
    return (
        <div className='smallCard'>
            <LargeTooltip title='Number of unique people who have seen your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Total Reach⠀
                    </Typography>
                    <RecordVoiceOverRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined' className='top'>
                <CardContent>
                    <Typography variant='body' component='h2' style={{ fontSize: 72 }}>
                        {number}
                    </Typography>
                </CardContent>
            </Card>
        </div>
    );
}

export const SentimentCard = ({ positive, posPercentage, negative, negPercentage, neutral, neutPercentage }) => {
    return (
        <div className='mediumCard'>
            <LargeTooltip title='Attitude and feelings people have about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Sentiment⠀
                    </Typography>
                    <FaceRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined' className='top'>
                <CardContent>
                    <div className='content' style={{ marginTop: '10px' }}>
                        <div className='sentiment'>
                            <Typography>
                                Positive
                            </Typography>
                            <Typography variant='body' component='h1' className='green'>
                                {positive}
                            </Typography>
                            <Typography color='textSecondary' className='percentage'>
                                ({posPercentage}%)
                            </Typography>
                        </div>
                        <div className='sentiment'>
                            <Typography>
                                Neutral
                            </Typography>
                            <Typography variant='body' component='h1'>
                                {neutral}
                            </Typography>
                            <Typography color='textSecondary' className='percentage'>
                                ({neutPercentage}%)
                            </Typography>
                        </div>
                        <div className='sentiment'>
                            <Typography>
                                Negative
                            </Typography>
                            <Typography variant='body' component='h1' className='red'>
                                {negative}
                            </Typography>
                            <Typography color='textSecondary' className='percentage'>
                                ({negPercentage}%)
                            </Typography>
                        </div>
                    </div>
                </CardContent>
            </Card>
        </div>
    );
}

export const ExposureCard = ({ exposureData }) => {
    return (
        <div className='fullCard'>
            <LargeTooltip title='Number of times your topic seen by people'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Exposure⠀
                    </Typography>
                    <BarChartRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined'>
                <CardContent>
                    <ExposureLineChart exposureData={exposureData}/>
                </CardContent>
            </Card>
        </div>
    );
}

export const LatestTweetsCard = ({ latestTweets }) => {
    return (
        <div className='fullCard'>
            <LargeTooltip title='Latest post about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Latest Tweets⠀
                    </Typography>
                    <TwitterIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined'>
                <CardContent>
                    <LatestTweetsTable latestTweets={latestTweets}/>
                </CardContent>
            </Card>
        </div>
    );
}

export const AgeCard = ({ ageProfile }) => {
    return (
        <div className='halfPageCard'>
            <LargeTooltip title='Age profile talking about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Age Profile⠀
                    </Typography>
                    <EventIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined' className='fullHeight'>
                <CardContent>
                    <AgeBarChart ageProfile={ageProfile} />
                </CardContent>
            </Card>
        </div>
    );
}

export const GenderCard = ({ femaleNumber, maleNumber }) => {
    return (
        <div className='halfPageCard'>
            <LargeTooltip title='Gender profile talking about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Gender Profile⠀
                    </Typography>
                    <WcRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined' className='fullHeight'>
                <CardContent>
                    <div className='genderBox'>
                        <div className='genderInner'>
                            <img src={female} alt='female icon' style={{ marginRight: '10px' }}></img>
                            <div>
                                <h2>Female</h2>
                                <p className='big'>{femaleNumber}</p>
                            </div>
                        </div>
                    </div>
                    <div className='genderBox'>
                    <div className='genderInner'>
                            <img src={male} alt='male icon' style={{ marginRight: '10px' }}></img>
                            <div>
                                <h2>Male</h2>
                                <p className='big'>{maleNumber}</p>
                            </div>
                        </div>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}

export const LocationCard = ({ topProvinces, topCities }) => {
    return (
        <div className='fullCard'>
            <LargeTooltip title='Location of people talking about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        User Location⠀
                    </Typography>
                    <LocationOnRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined'>
                <CardContent>
                    <div>
                        <ChoroplethMap data={topProvinces} />
                    </div>
                    <div className='content'>
                        <div className='halfSize'>
                            <LocationBarChart topProvinces={topProvinces} />
                        </div>
                        <div className='halfSize'>
                            <LocationDoughnutChart topCities={topCities} />
                        </div>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}

export const KeyOpinionLeaderCard = ({ positiveKOL, negativeKOL }) => {
    return (
        <div className='fullCard'>
            <LargeTooltip title='Person with most post with retweet / comment about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Key Opinion Leader⠀
                    </Typography>
                    <PeopleOutlineRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined'>
                <CardContent>
                    <div className='content'>
                        <div className='halfTable'>
                            <Typography variant='body' component='h2' style={{ margin: '10px' }}>
                                Positive
                            </Typography>
                            <PositiveReachTable entries={positiveKOL} />
                        </div>
                        <div className='halfTable'>
                            <Typography variant='body' component='h2' style={{ margin: '10px' }}>
                                Negative
                            </Typography>
                            <NegativeReachTable entries={negativeKOL} />
                        </div>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}

export const MostActiveUsersCard = ({ positiveMAU, negativeMAU }) => {
    return (
        <div className='fullCard'>
            <LargeTooltip title='Person with most posts about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Most Active Users⠀
                    </Typography>
                    <PeopleOutlineRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined'>
                <CardContent>
                    <div className='content'>
                        <div className='halfTable'>
                            <Typography variant='body' component='h2' style={{ margin: '10px' }}>
                                Positive
                            </Typography>
                            <PositiveReachTable entries={positiveMAU} type='mau' />
                        </div>
                        <div className='halfTable'>
                            <Typography variant='body' component='h2' style={{ margin: '10px' }}>
                                Negative
                            </Typography>
                            <NegativeReachTable entries={negativeMAU} type='mau' />
                        </div>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}

export const NGramCard = ({ page, nGramData }) => {
    return (
        <div className='fullCard'>
            <LargeTooltip title='Most popular word about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        N-Gram⠀
                    </Typography>
                    <BarChartRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined'>
                <CardContent>
                    <NGramBarChart nGramData={nGramData} />
                    <br />
                    {page === 'dashboard' ? <TweetSampleTable entries={nGramData} /> : <NewsSampleTable entries={nGramData} />}
                </CardContent>
            </Card>
        </div>
    );
}

export const TotalNewsCard = ({ number }) => {
    return (
        <div className='mediumCard'>
            <LargeTooltip title='Number of news talking about your topic '>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Total News⠀
                    </Typography>
                    <RecordVoiceOverRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined' className='top'>
                <CardContent>
                    <Typography variant='body' component='h2' style={{ fontSize: 72 }}>
                        {number}
                    </Typography>
                </CardContent>
            </Card>
        </div>
    );
}

export const NewsTrendCard = ({ newsTrend }) => {
    return (
        <div className='fullCard'>
            <LargeTooltip title='Daily number of news about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        News Trend⠀
                    </Typography>
                    <BarChartRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>            
            <Card variant='outlined'>
                <CardContent>
                    <NewsTrendLineChart newsTrend={newsTrend} />
                </CardContent>
            </Card>
        </div>
    );
}

export const LatestNewsCard = ({ latestNews }) => {
    return (
        <div className='fullCard'>
            <LargeTooltip title='Latest post about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Latest News⠀
                    </Typography>
                    <BarChartRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined'>
                <CardContent>
                    <LatestNewsTable latestNews={latestNews} />
                </CardContent>
            </Card>
        </div>
    );
}

export const MediaCoverageCard = ({ mediaCoverage }) => {
    return (
        <div className='fullCard'>
            <LargeTooltip title='Media with most readers about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Media Coverage⠀
                    </Typography>
                    <BarChartRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined'>
                <CardContent>
                    <MediaCoverageBarChart mediaCoverage={mediaCoverage} />
                </CardContent>
            </Card>
        </div>
    );
}

export const NewsSentimentCard = ({ positive, posPercentage, negative, negPercentage, neutral, neutPercentage }) => {
    return (
        <div className='largeCard'>
            <LargeTooltip title='Attitude and feelings people have about your topic'>
                <div className='floatingHeader'>
                    <Typography variant='body' className='bold'>
                        Sentiment⠀
                    </Typography>
                    <FaceRoundedIcon fontSize='medium' />
                </div>
            </LargeTooltip>
            <Card variant='outlined' className='top'>
                <CardContent>
                    <div className='content' style={{ marginTop: '10px' }}>
                        <div className='sentiment'>
                            <Typography>
                                Positive
                            </Typography>
                            <Typography variant='body' component='h1' className='green'>
                                {positive}
                            </Typography>
                            <Typography color='textSecondary' className='percentage'>
                                ({posPercentage}%)
                            </Typography>
                        </div>
                        <div className='sentiment'>
                            <Typography>
                                Neutral
                            </Typography>
                            <Typography variant='body' component='h1'>
                                {neutral}
                            </Typography>
                            <Typography color='textSecondary' className='percentage'>
                                ({neutPercentage}%)
                            </Typography>
                        </div>
                        <div className='sentiment'>
                            <Typography>
                                Negative
                            </Typography>
                            <Typography variant='body' component='h1' className='red'>
                                {negative}
                            </Typography>
                            <Typography color='textSecondary' className='percentage'>
                                ({negPercentage}%)
                            </Typography>
                        </div>
                    </div>
                </CardContent>
            </Card>
        </div>
    );
}