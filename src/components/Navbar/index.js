import React, { useEffect } from 'react'
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { Nav, NavbarLogo, NavbarProfile } from './elements';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { GoogleLogout } from 'react-google-login';
import Cookies from 'js-cookie';
import { useHistory } from 'react-router-dom';
import { axiosInstance } from '../../services/api';
import logo from '../../images/logo1.png';
import config from '../../config';

function Navbar() {
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);

    const [membership, setMembership] = React.useState('')
    const [payment, setPayment] = React.useState('')

    const getProfile = async() => {
        try {
            const res = await axiosInstance.get('auth/profile/')
            console.log(res)
            setMembership(res.data.data.user.membership)
            setPayment(res.data.data.payment.payment_status)
        } catch(err) {
            console.log(err)
        }
    }

    useEffect(() => {
        getProfile();
    }, []);

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
          return;
        }
        setOpen(false);
    };

    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            setOpen(false);
        }
    }

    const history = useHistory()

    const handleSubmit = (e) => {
        e.preventDefault();

        setOpen(false);
        
        axiosInstance
            .post('auth/logout/', {
                refresh: Cookies.get('refresh')
            })
            .then((res) => {
                Cookies.remove('access')
                Cookies.remove('refresh')
                Cookies.remove('login_type')
                console.log(res)
                history.push('/login')
            })
            .catch((err) => {
                console.log(err);
            });
    }

    const logout = (response) => {
        console.log(response);
        Cookies.remove('access')
        Cookies.remove('refresh')
        Cookies.remove('login_type')
        history.push('/login')
    }

    const isLoggedIn = Cookies.get('refresh') ? true : false
    const loginType = Cookies.get('login_type')

    return (
        <Nav>
            <NavbarLogo onClick={() => {window.location.pathname = '/'}}>
                <img src={logo} alt='logo' style={{ height: '45px', marginTop: '5px' }}/>
            </NavbarLogo>
            {isLoggedIn ?
                <NavbarProfile>
                    {membership === 'Premium' && payment !== 'Pending' ?
                        <p style={{ fontSize: '17px', marginTop: '8px', fontWeight: 'bold', color: '#1d2691' }}> PREMIUM </p> :
                        undefined
                    }
                    {membership === 'Free' && payment !== 'Pending' ?
                        <Button
                        style={{ marginRight: '10px' }}
                        variant='contained'
                        color='primary'
                        onClick={() => {window.location.pathname = '/upgrade/'}}
                        >
                            Upgrade Membership
                        </Button> :
                        undefined
                    }
                    {payment === 'Pending' ?
                        <Button
                        style={{ marginRight: '10px' }}
                        variant='contained'
                        color='primary'
                        onClick={() => {window.location.pathname = '/upgrade/payment/'}}
                        >
                            Upgrade Membership
                        </Button> :
                        undefined
                    }
                    <Button
                    ref={anchorRef}
                    aria-controls={open ? 'menu-list-grow' : undefined}
                    aria-haspopup='true'
                    onClick={handleToggle}
                    >
                        <AccountCircleIcon style={{ color: '#4047FF' }}/>
                    </Button>
                    <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                    {({ TransitionProps, placement }) => (
                        <Grow
                        {...TransitionProps}
                        style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                        >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                            <MenuList autoFocusItem={open} id='menu-list-grow' onKeyDown={handleListKeyDown}>
                                <MenuItem onClick={() => {window.location.pathname = '/profile'}}>Profile</MenuItem>
                                {loginType === 'email' ? 
                                    <MenuItem onClick={handleSubmit}>Logout</MenuItem> :
                                    <GoogleLogout
                                        icon={false}
                                        clientId={config.GOOGLE_CLIENT_ID}
                                        render={renderProps => (
                                            <MenuItem onClick={renderProps.onClick}>Logout</MenuItem>
                                        )}
                                        buttonText='Logout'
                                        onLogoutSuccess={logout}
                                    >
                                    </GoogleLogout>
                                }
                            </MenuList>
                            </ClickAwayListener>
                        </Paper>
                        </Grow>
                    )}
                    </Popper>
                </NavbarProfile> : undefined
            }
        </Nav>
    )
}

export default Navbar
