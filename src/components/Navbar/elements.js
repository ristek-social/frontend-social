import styled from 'styled-components'

export const Nav = styled.nav`
    height: 55px;
    width: 100%;
    display: flex;
    background-color: white;
    position: fixed;
    top: 0;
    z-index: 999;
    box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.25);
`

export const NavbarLogo = styled.div`
    margin-left: 20px;
    position: absolute;
    line-height: 55px;
    cursor: pointer;
`

export const NavbarProfile = styled.div`
    display: flex;
    flex-direction: row;
    float: right;
    right: 0;
    position: absolute;
    margin: 8px 10px auto auto;
`