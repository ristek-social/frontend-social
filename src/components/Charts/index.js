import React from 'react';
import { Bar, Line, Doughnut } from 'react-chartjs-2'

const orange = '#ffa500'
const orangeTransparent = '#ffa50080'

const primaryBackground = [
    '#4140dd',
    '#4847f6',
    '#5a59f7',
    '#6d6cf8',
    '#7f7ef9',
    '#9191fa',
    '#a4a3fb',
    '#b6b5fb',
    '#c8c8fc',
    '#dadafd',
    '#ededfe',
]

export const ExposureLineChart = ({ exposureData }) => {
    var dates = []
    var frequencies = []

    for (let i = 0; i < exposureData.length; i++) {
        const convertedDate = (new Date(exposureData[i].date))
                                .toLocaleString('default', { year: 'numeric', month: 'short', day: 'numeric', });
        dates.push(convertedDate);
        frequencies.push(exposureData[i].exposure);
    }

    return (
        <div>
            <Line
                data={{
                labels: dates,
                datasets: [
                    {
                    label: 'Daily Exposure',
                    data: frequencies,
                    backgroundColor: [
                        orangeTransparent,
                    ],
                    borderColor: [
                        orange,
                    ],
                    borderWidth: 1,
                    },
                ],
                }}
                height={400}
                width={600}
                options={{
                maintainAspectRatio: false,
                scales: {
                    yAxes: [
                    {
                        ticks: {
                        beginAtZero: true,
                        },
                    },
                    ],
                },
                legend: {
                    labels: {
                    fontSize: 25,
                    },
                },
                }}
            />
        </div>
    );
}

export const NGramBarChart = ({ nGramData }) => {
    var keywords = []
    var frequencies = []

    for (let i = 0; i < nGramData.length; i++) {
        keywords.push(nGramData[i].keyword);
        frequencies.push(nGramData[i].frequency);
    }
    return (
        <div>
            <Bar
                data={{
                labels: keywords,
                datasets: [
                    {
                    label: 'Total Entries',
                    data: frequencies,
                    backgroundColor: [
                        'rgba(0, 10, 255, 0.2)',
                    ],
                    borderColor: [
                        'rgba(0, 10, 255, 1)',
                    ],
                    borderWidth: 1,
                    },
                ],
                }}
                height={400}
                width={600}
                options={{
                indexAxis: 'y',
                maintainAspectRatio: false,
                scales: {
                    yAxes: [
                    {
                        ticks: {
                        beginAtZero: true,
                        },
                    },
                    ],
                },
                legend: {
                    labels: {
                    fontSize: 25,
                    },
                },
                }}
            />
        </div>
    );
}

export const NewsTrendLineChart = ({ newsTrend }) => {
    var dates = []
    var frequencies = []

    for (let i = 0; i < newsTrend.length; i++) {
        const convertedDate = (new Date(newsTrend[i].date))
                                .toLocaleString('default', { year: 'numeric', month: 'short', day: 'numeric', });
        dates.push(convertedDate);
        frequencies.push(newsTrend[i].daily_trend);
    }

    return (
        <div>
            <Line
                data={{
                labels: dates,
                datasets: [
                    {
                    label: 'News',
                    data: frequencies,
                    backgroundColor: [
                        orangeTransparent,
                    ],
                    borderColor: [
                        orange,
                    ],
                    borderWidth: 1,
                    },
                ],
                }}
                height={400}
                width={600}
                options={{
                maintainAspectRatio: false,
                scales: {
                    yAxes: [
                    {
                        ticks: {
                        beginAtZero: true,
                        },
                    },
                    ],
                },
                legend: {
                    labels: {
                    fontSize: 25,
                    },
                },
                }}
            />
        </div>
    );
}

export const MediaCoverageBarChart = ({ mediaCoverage }) => {
    var medias = []
    var frequencies = []

    for (let i = 0; i < mediaCoverage.length; i++) {
        medias.push(mediaCoverage[i].media)
        frequencies.push(mediaCoverage[i].frequency)
    }

    return (
        <div>
            <Bar
                data={{
                labels: medias,
                datasets: [
                    {
                    label: 'Total Exposure',
                    data: frequencies,
                    backgroundColor: [
                        'rgba(0, 10, 255, 0.2)',
                    ],
                    borderColor: [
                        'rgba(0, 10, 255, 1)',
                    ],
                    borderWidth: 1,
                    },
                ],
                }}
                height={400}
                width={600}
                options={{
                indexAxis: 'y',
                maintainAspectRatio: false,
                scales: {
                    yAxes: [
                    {
                        ticks: {
                        beginAtZero: true,
                        },
                    },
                    ],
                },
                legend: {
                    labels: {
                    fontSize: 25,
                    },
                },
                }}
            />
        </div>
    );
}

export const AgeBarChart = ({ ageProfile }) => {
    var ageRanges = []
    var frequencies = []

    for (let i = 0; i < ageProfile.length; i++) {
        ageRanges.push(ageProfile[i].range)
        frequencies.push(ageProfile[i].frequency)
    }

    return (
        <div>
            <Bar
                data={{
                labels: ageRanges,
                datasets: [
                    {
                    label: 'Number of People',
                    data: frequencies,
                    backgroundColor: [
                        'rgba(0, 10, 255, 0.2)',
                    ],
                    borderColor: [
                        'rgba(0, 10, 255, 1)',
                    ],
                    borderWidth: 1,
                    },
                ],
                }}
                height={400}
                width={600}
                options={{
                maintainAspectRatio: false,
                scales: {
                    yAxes: [
                    {
                        ticks: {
                        beginAtZero: true,
                        },
                    },
                    ],
                },
                legend: {
                    labels: {
                    fontSize: 25,
                    },
                },
                }}
            />
        </div>
    );
}

export const LocationBarChart = ({ topProvinces }) => {
    const provinceCodes = {
        AC: 'DI Aceh', KI: 'Kalimantan Timur', JR: 'Jawa Barat',
        JT: 'Jawa Tengah', BE: 'Bengkulu', BT: 'Banten',
        JK: 'DKI Jakarta', KB: 'Kalimantan Barat', LA: 'Lampung',
        SL: 'Sumatera Selatan', BB: 'Bangka-Belitung', BA: 'Bali',
        JI: 'Jawa Timur', KS: 'Kalimantan Selatan', NT: 'Nusa Tenggara Timur',
        SE: 'Sulawesi Selatan', SR: 'Sulawesi Barat', KR: 'Kepulauan Riau',
        GO: 'Gorontalo', JA: 'Jambi', KT: 'Kalimantan Tengah',
        IB: 'Irian Jaya Barat', SU: 'Sumatera Utara', RI: 'Riau',
        SW: 'Sulawesi Utara', MU: 'Maluku Utara', SB: 'Sumatera Barat',
        YO: 'Yogyakarta', MA: 'Maluku', NB: 'Nusa Tenggara Barat',
        SG: 'Sulawesi Tenggara', ST: 'Sulawesi Tengah', PA: 'Papua', 
    }

    var provinces = []
    var frequencies = []

    for (let i = 0; i < topProvinces.length; i++) {
        provinces.push(provinceCodes[topProvinces[i].name] || topProvinces[i].name)
        frequencies.push(topProvinces[i].frequency)
    }
    return (
        <div>
            <Bar
                data={{
                labels: provinces,
                datasets: [
                    {
                    label: 'Total Exposure',
                    data: frequencies,
                    backgroundColor: [
                        '#2b2b94',
                        '#3a39c5',
                        '#4140dd',
                        '#4847f6',
                        '#5a59f7',
                        '#7f7ef9',
                        '#9191fa',
                        '#a4a3fb',
                        '#b6b5fb',
                        '#c8c8fc',
                        '#ededfe'
                    ],
                    // borderColor: primaryBorder,
                    // borderWidth: 1,
                    },
                ],
                }}
                height={300}
                width={450}
                options={{
                indexAxis: 'y',
                maintainAspectRatio: false,
                scales: {
                    yAxes: [
                    {
                        ticks: {
                        beginAtZero: true,
                        },
                    },
                    ],
                },
                legend: {
                    labels: {
                    fontSize: 25,
                    },
                },
                }}
            />
        </div>
    );
}

export const LocationDoughnutChart = ({ topCities }) => {
    var cities = []
    var frequencies = []

    for (let i = 0; i < topCities.length; i++) {
        cities.push(topCities[i].name)
        frequencies.push(topCities[i].frequency)
    }
    return (
        <div>
            <Doughnut
                data={{
                labels: cities,
                datasets: [
                    {
                    label: 'Top Cities',
                    data: frequencies,
                    backgroundColor: primaryBackground,
                    borderColor: [
                        '#4140dd'
                    ],
                    borderWidth: 1,
                    },
                ],
                }}
                height={200}
                width={300}
                options={{
                maintainAspectRatio: false,
                legend: {
                    labels: {
                    fontSize: 25,
                    },
                },
                }}
            />
        </div>
    );
}