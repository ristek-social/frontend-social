import React from 'react'
import mail from '../../images/mail.svg'
import instagram from '../../images/instagram.svg'

function Footer() {
    return (
        <footer>
            <div className='footerContact'>
                <div className='footerContactItem'>
                    <img src={mail} alt='mail logo' /><a href='mailto:contact@continuum.id' target='_blank' className='mail'>contact@continuum.id</a>
                </div>
                <div className='footerContactItem'>
                    <img src={instagram} alt='instagram logo'/><a href='https://www.instagram.com/continuum.data/' target='_blank' className='ig'>@continuum.data</a>
                </div>
            </div>
            <p>Copyright © 2021. All rights reserved by Continuum Data Indonesia.</p>
        </footer>
    )
}

export default Footer
