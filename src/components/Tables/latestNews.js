import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

const columns = [
    { id: 'no', label: 'No', minWidth: 50 },
    { id: 'date', label: 'Date', minWidth: 225, },
    { id: 'sentiment', label: 'Sentiment', minWidth: 75 },
    { id: 'media', label: 'Media', minWidth: 75 },
    { id: 'title', label: 'Title', minWidth: 150 },
    { id: 'link', label: 'Link', minWidth: 200 },
];

const useStyles = makeStyles({
  root: {
    width: '100%',
    fontFamily: 'Roboto',
    color: 'red'
  },
  container: {
    maxHeight: 440,
  },
});

export default function LatestNewsTable({ latestNews }) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage] = React.useState(10);

  var no = 0;

  function createData(date, sentiment, media, title, link) {
    no += 1
    date = (new Date(date)).toLocaleString(
      undefined, { 
        weekday: 'short',
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit'
      }
    )
    return { no, date, sentiment, media, title, link };
  }

  const rows = [];

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  for (let i = 0; i < latestNews.length; i++) {
    const latest = latestNews[i];
    rows.push(createData(latest.date, latest.sentiment, latest.media, latest.title, latest.link))
  }

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth, fontFamily: 'Roboto' }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role='checkbox' tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    if (column.id === 'link') {
                        return (
                            <TableCell key={column.id} align={column.align}>
                                <a href={value} target='_blank' style={{ fontFamily: 'Roboto' }}>
                                    {value}
                                </a>
                            </TableCell>
                        )
                    }
                    return (
                      <TableCell key={column.id} align={column.align} style={{ fontFamily: 'Roboto' }}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        component='div'
        count={rows.length}
        rowsPerPage={10}
        rowsPerPageOptions={[10]}
        page={page}
        onPageChange={handleChangePage}
      />
    </Paper>
  );
}
