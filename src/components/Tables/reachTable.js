import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

const kolColumns = [
  { id: 'no', label: 'No', minWidth: 50 },
  { id: 'account', label: 'Account', minWidth: 75 },
  { id: 'reach', label: 'Total Reach', minWidth: 200, align: 'right'}
];

const mauColumns = [
  { id: 'no', label: 'No', minWidth: 50 },
  { id: 'account', label: 'Account', minWidth: 75 },
  { id: 'reach', label: 'Total Posts', minWidth: 200, align: 'right'}
];

const columns = [
  { id: 'no', label: 'No', minWidth: 50 },
  { id: 'account', label: 'Account', minWidth: 75 },
  { id: 'reach', label: 'Total Reach', minWidth: 200, align: 'right'}
];

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
});

export function PositiveReachTable({ entries, type }) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage] = React.useState(5);

  var no = 0;

  function createData(account, reach) {
    no += 1
    return { no, account, reach };
  }

  const rows = [];

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  for (let i = 0; i < entries.length; i++) {
    const entry = entries[i];
    rows.push(createData(entry.account, entry.frequency))
  }

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
            { type === 'mau' ?
              <TableRow>
                {mauColumns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth, borderColor: '#00ce9c', borderWidth: '3px', fontFamily: 'Roboto' }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow> :
              <TableRow>
                {kolColumns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth, borderColor: '#00ce9c', borderWidth: '3px', fontFamily: 'Roboto' }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            }
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role='checkbox' tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align} style={{ fontFamily: 'Roboto' }}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        component='div'
        count={rows.length}
        rowsPerPage={5}
        rowsPerPageOptions={[5]}
        page={page}
        onPageChange={handleChangePage}
      />
    </Paper>
  );
}

export function NegativeReachTable({ entries, type }) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage] = React.useState(5);

  var no = 0;

  function createData(account, reach) {
    no += 1
    return { no, account, reach };
  }

  const rows = [];

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  for (let i = 0; i < entries.length; i++) {
    const entry = entries[i];
    rows.push(createData(entry.account, entry.frequency))
  }

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
          { type === 'mau' ?
              <TableRow>
                {mauColumns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth, borderColor: '#f93822', borderWidth: '3px', fontFamily: 'Roboto' }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow> :
              <TableRow>
                {kolColumns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth, borderColor: '#f93822', borderWidth: '3px', fontFamily: 'Roboto' }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            }
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role='checkbox' tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align} style={{ fontFamily: 'Roboto' }}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        component='div'
        count={rows.length}
        rowsPerPage={5}
        rowsPerPageOptions={[5]}
        page={page}
        onPageChange={handleChangePage}
      />
    </Paper>
  );
}
