import React, { useEffect, useState } from 'react';
import Datamap from 'datamaps/dist/datamaps.world.js';
import * as d3 from 'd3';
import IndonesiaJson from './Indonesia.topo.json';
import '../../App.css'

function ChoroplethMap({ data }) {
    let dataset = {};

    let palette = [
        '#2b2b94',
        '#3a39c5',
        '#4140dd',
        '#4847f6',
        '#5a59f7',
        '#7f7ef9',
        '#9191fa',
        '#a4a3fb',
        '#b6b5fb',
        '#c8c8fc',
    ]

    const [loaded, setLoaded] = useState(false);

    for (let i = 0; i < data.length; i++) {
        if (i < 10) {
            let iso = data[i].name,
                value = data[i].frequency;
            dataset[iso] = { numberOfThings: value, fillColor: palette[i] }
        }
    }

    useEffect(() => {
        if (data.length > 0) {
            document.getElementById('choropleth_map').innerHTML = "";
            let map = new Datamap({
                element: document.getElementById('choropleth_map'),
                scope: 'indonesia',
                geographyConfig: {
                    popupOnHover: false,
                    highlightOnHover: false,
                    borderColor: '#444',
                    borderWidth: 0.5,
                    dataJson: IndonesiaJson,
                },
                fills: {
                    UNKNOWN: '#ededfe80',
                    defaultFill: '#ededfe80'
                },
                data: dataset,
                setProjection: function (element) {
                    var projection = d3.geoMercator()
                        .center([113.9213, -0.7893]) // always in [East Latitude, North Longitude]
                        .scale(900)
                        .translate([element.offsetWidth / 2, element.offsetHeight / 2]);
                    console.log(dataset)
                    var path = d3.geoPath().projection(projection);
                    return { path: path, projection: projection };
                }
            });
            setLoaded(true);
        }
    }, [data])

    return (
        <div id='choropleth_map'></div>
    );
}

export default ChoroplethMap;