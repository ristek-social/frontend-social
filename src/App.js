import { BrowserRouter as Router, Switch } from 'react-router-dom';
import './App.css';
import Register from './pages/Register';
import EmailVerification from './pages/Register/emailVerification';
import Login from './pages/Login';
import TopicList from './pages/TopicList';
import AddTopic from './pages/AddTopic';
import Dashboard from './pages/Dashboard';
import Media from './pages/Media';
import Profile from './pages/Profile';
import UpgradeMembership from './pages/UpgradeMembership';
import Payment from './pages/UpgradeMembership/payment';
import { PrivateRoute, PublicRoute } from './Routes';
import Footer from './components/Footer';

function App() {
  return (
    <>
    <div className='wrapper'>
      <Router>
        <Switch>
          <PublicRoute path='/register' name='register' exact component={Register} />
          <PublicRoute path='/verify' name='emailVerification' exact component={EmailVerification} />
          <PublicRoute path='/login' name='login' exact component={Login} />
          <PrivateRoute path='/' name='topicList' exact component={TopicList} />
          <PrivateRoute path='/add-topic' name='addTopic' exact component={AddTopic} />
          <PrivateRoute path='/dashboard/:id' name='dashboard' exact component={Dashboard} />
          <PrivateRoute path='/media/:id' name='media' exact component={Media} />
          <PrivateRoute path='/profile' name='profile' exact component={Profile} />
          <PrivateRoute path='/upgrade' name='upgrade' exact component={UpgradeMembership} />
          <PrivateRoute path='/upgrade/payment' name='payment' exact component={Payment} />
        </Switch>
      </Router>
    </div>
    <div class="push"></div>
    <Footer />
    </>
  );
}

export default App;
