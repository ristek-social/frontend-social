import axios from 'axios';
import Cookies from 'js-cookie';
import config from '../config';

export const authInstance = axios.create({
    baseURL: config.API_BASE_URL
})

export const axiosInstance = axios.create({
    baseURL: config.API_BASE_URL,
    headers: {
        'Content-Type': 'application/json'
    }
})

axiosInstance.interceptors.request.use(
    (config) => {
        const token = Cookies.get('access')
        if (token && config.url !== '/auth/register' && config.url !== '/auth/login') {
            config.headers['Authorization'] = 'Bearer ' + token;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
)

axiosInstance.interceptors.response.use(
    (res) => {
        return res;
    },
    async (err) => {
        const originalConfig = err.config;

        if (originalConfig.url === '/auth/token/refresh/') {
            Cookies.remove('access')
            Cookies.remove('refresh')
            window.location.href = '/login'
        } else if (err.response) {
            console.log(err.config)
            console.log(originalConfig.url)
            console.log()
            if (err.response.status === 401 && !originalConfig._retry) {
                originalConfig._retry = true;

                try {
                    const rs = await axiosInstance.post('/auth/token/refresh/', {
                        refresh: Cookies.get('refresh')
                    });

                    const accessToken = rs.data.access;
                    Cookies.set('access', accessToken);
                    originalConfig.headers['Authorization'] = 'Bearer ' + accessToken;

                    return axiosInstance(originalConfig);
                } catch (_error) {
                    return Promise.reject(_error);
                }
            }
        }

        return Promise.reject(err);
    }
)